function debugLog() {
  Java.perform(function() {
    const LogConfig = Java.use('com.sumup.android.logging.LogConfig');
    LogConfig.setDeobfuscateMode(true);
    LogConfig.setLogLevel('VERBOSE');
  });
}

Java.perform(function() {
  const ReaderModuleCoreState = Java.use(
    'com.sumup.merchant.reader.ReaderModuleCoreState',
  );
  const EmvCardReaderController = Java.use(
    'com.sumup.merchant.reader.controllers.EmvCardReaderController',
  );
  const PinPlusReaderDevice = Java.use(
    'com.sumup.reader.core.Devices.PinPlusReaderDevice',
  );
  const ReaderCoreManager = Java.use(
    'com.sumup.merchant.reader.cardreader.ReaderCoreManager',
  );
  const CardReaderDeviceInfo = Java.use(
    'com.sumup.reader.core.model.CardReaderDeviceInfo',
  );
  const Reader = Java.use('com.sumup.reader.core.model.Reader');
  const ConnectionMode = Java.use('com.sumup.reader.core.model.ConnectionMode');
  const CardReaderDevice = Java.use(
    'com.sumup.reader.core.Devices.CardReaderDevice',
  );

  const instance = ReaderModuleCoreState.Instance();
  const emvCardReaderController = Java.cast(
    instance.get(EmvCardReaderController.class),
    EmvCardReaderController,
  );
  const readerCoreManager = Java.cast(
    instance.get(ReaderCoreManager.class),
    ReaderCoreManager,
  );

  PinPlusReaderDevice.getDeviceInfo.implementation = function() {
    console.log('getDeviceInfo called');
    const DeviceId = Java.use(
      'com.sumup.reader.core.Devices.CardReaderDevice$DeviceId',
    );
    const Type = Java.use('com.sumup.reader.core.model.Reader$Type');
    const Integer = Java.use('java.lang.Integer');
    const deviceId = DeviceId.ID_SUMUP.value;
    const type = Type.PIN_PLUS.value;
    const connectionMode = ConnectionMode.BLUETOOTH_SMART.value;

    const info = CardReaderDeviceInfo.$new(
      deviceId,
      80,
      'b9',
      '1.2.28.8',
      Integer.valueOf(16915464),
      Integer.valueOf(336600),
      'pin+',
      type,
      connectionMode,
    );
    console.log('info', info);
    return info;
  };

  const pinPlusReaderDevice = PinPlusReaderDevice.$new(null, null, null);
  const map = Java.use('java.util.HashMap').$new();
  const cardReaderDevice = Java.cast(pinPlusReaderDevice, CardReaderDevice);
  //console.log('emvCardReaderController.class', emvCardReaderController.class);
  //console.log(emvCardReaderController.doTxCheckout());
  //console.log(emvCardReaderController.sendDeviceInfo());
  console.log(
    emvCardReaderController.startNewTransaction(
      cardReaderDevice,
      readerCoreManager,
      map,
    ),
  );
});

Java.perform(function() {
  const ReaderModuleCoreState = Java.use(
    'com.sumup.merchant.reader.ReaderModuleCoreState',
  );
  const CardReaderPaymentFlowController = Java.use(
    'com.sumup.merchant.reader.controllers.CardReaderPaymentFlowController',
  );
  const instance = ReaderModuleCoreState.Instance();
  const cardReaderPaymentFlowController = Java.cast(
    instance.get(CardReaderPaymentFlowController.class),
    CardReaderPaymentFlowController,
  );
  console.log(
    'cardReaderPaymentFlowController',
    cardReaderPaymentFlowController,
  );
});

Java.perform(function() {
  const CardReaderDeviceInfo = Java.use(
    'com.sumup.reader.core.model.CardReaderDeviceInfo',
  );
  const ConnectionMode = Java.use('com.sumup.reader.core.model.ConnectionMode');
  const DeviceId = Java.use(
    'com.sumup.reader.core.Devices.CardReaderDevice$DeviceId',
  );
  const Type = Java.use('com.sumup.reader.core.model.Reader$Type');
  const Integer = Java.use('java.lang.Integer');

  const deviceId = DeviceId.ID_SUMUP.value;
  const type = Type.PIN_PLUS.value;
  const connectionMode = ConnectionMode.BLUETOOTH_SMART.value;

  const info = CardReaderDeviceInfo.$new(
    deviceId,
    0,
    '',
    '',
    Integer.valueOf(0),
    Integer.valueOf(0),
    '',
    type,
    connectionMode,
  );
  console.log('info', info);
});

Java.perform(function() {
  const PinPlusReaderDevice = Java.use(
    'com.sumup.reader.core.Devices.PinPlusReaderDevice',
  );
  PinPlusReaderDevice.setUnprotectedCommands.implementation = function(list) {
    console.log('setUnprotectedCommands', JSON.stringify(list));
  };
});

Java.perform(function() {
  const PinPlusReaderDevice = Java.use(
    'com.sumup.reader.core.Devices.PinPlusReaderDevice',
  );
  PinPlusReaderDevice.getDeviceInfo.implementation = null;
});
