# SumUp

Hold 3 + power while powering up connected to USB

- Boot

  > LD AIR v2
  > 3.0.13.14

- Config
  Info Version
  DEV SN: 300350925
  USIP SN: B26786210800d6af

## References

- https://sumup.en.uptodown.com/android/versions
- https://github.com/fjl/hello
- https://frw.sumup.com/man/speed_configuration-android.json
- https://frw.sumup.com/svpp-1.2.28.8.bin
- https://frw.sumup.com/svpp-1.2.28.8.sig

## Hardware

MAX32550

## API

### Notes

JSON-RPC 2.0

https://triangle-ng.sumup.com/api/0.1/

> http POST https://triangle-ng.sumup.com/api/0.1/auth id="0" method="login" params:='{"username": "giyib97890@astarmax.com", "password": "Wells123", "hashed": false}'

"message": "The version of the app you are using is outdated, please upgrade to the latest version."

http POST https://triangle-ng.sumup.com/api/0.1/auth id="0" method="login" params:='{"app": {"id": "com.sumup.merchant.reader.sdk", "version": "210601027", "version_name": "3.3.1"}, "username": "giyib97890@astarmax.com", "password": "Wells123", "hashed": false}'

```
{
    "id": "0",
    "jsonrpc": "2.0",
    "result": {
        "accesstoken": "0bf5f4f88e0d0d08409ef060376d8f8742cf3f6bfc496976cad12ce20f01fe2a",
        "business": {
            "address_line_1": "206  Aviation Way",
            "business_name": "giyib97890",
            "city": "Dallas",
            "country": {
                "currency": {
                    "code": "USD",
                    "description": "US Dollar"
                },
                "en_name": "United States of America",
                "iso_code": "US"
            },
            "id": 0,
            "merchant_category": {
                "code": "8699"
            },
            "merchant_code": "M9CCD77F",
            "post_code": "75247",
            "region": {
                "name": "Texas"
            }
        },
```

---

http -v POST https://txg-ng.sumup.com/api/reader/get_fw_update/v1 jsonrpc="2.0" id="com.sumup.merchant.reader.sdk" method="get_firmware_update" params:='{"app": {"id": "com.sumup.merchant.reader.sdk", "version": "210601027", "version_name": "3.3.1"}, "accesstoken": "0bf5f4f88e0d0d08409ef060376d8f8742cf3f6bfc496976cad12ce20f01fe2a", "reader": {"serial_number": "b267", "firmware_vsn": "1.0.1.66", "firmware_vsn_int": 16777538, "firmware_identifier": "pin+", "basefirmware_vsn_int": 50336610, "battery_level": 85, "connection_type": "btle"}}'

```
{
    "id": "com.sumup.merchant.reader.sdk",
    "jsonrpc": "2.0",
    "result": {
        "firmware": [
            {
                "firmware_url": "https://frw.sumup.com/svpp-1.2.28.8.bin",
                "firmware_vsn_int": 16915464,
                "mandatory_by": null,
                "signature_url": "https://frw.sumup.com/svpp-1.2.28.8.sig"
            }
        ],
        "up_to_date": false
    }
}
```

Certificate in commands from server to device

```
-----BEGIN CERTIFICATE-----
MIIDSDCCAjACCQC+BKp7u63eFDANBgkqhkiG9w0BAQsFADBcMRswGQYDVQQKExJT
dW1VcCBQYXltZW50cyBMdGQxDzANBgNVBAcTBkJlcmxpbjEPMA0GA1UECBMGQmVy
bGluMQswCQYDVQQGEwJERTEOMAwGA1UEAxMFU3VtVXAwHhcNMTYwMTE0MDk0MjI1
WhcNMjEwNzA2MDk0MjI1WjBwMQswCQYDVQQGEwJERTEPMA0GA1UECAwGQmVybGlu
MQ8wDQYDVQQHDAZCZXJsaW4xGzAZBgNVBAoMElN1bVVwIFBheW1lbnRzIEx0ZDEP
MA0GA1UECwwGcGF5c2VjMREwDwYDVQQDDAhIU00gQXV0aDCCASIwDQYJKoZIhvcN
AQEBBQADggEPADCCAQoCggEBAL6ttRm17S9s6ndRuJo9vFvofXG1b6hlDNWSF6Yh
fsgq4qlIcdLRhWGjd1zPERJdEdRlrPlLlOxDAoNv6xInNAi980ZepPHLrHcxuulJ
MUS33tRczdubMBX3xMfCEDGWNS4/FRyddIn+Eyn5SuInuabFXpjy59AujwYTi8kW
+twMJ69mMjN7eGaM1sxnxSJ47CuPtCDgIMUfLMr+RnoFrFOTbn4dijo4Ej3ClCxQ
e+ubut4KEpMINmt26fdAjvkKgr+sv/JlDx3tMG5zJwV4LHnispkwHnxzKOOBirSb
49YwsQrtnBO5HoX15vtgVbbolqomPMQHG+H25zJ2jUH8xvUCAwEAATANBgkqhkiG
9w0BAQsFAAOCAQEAd4wYku1pV+2idKGSJZF07PbD01/REvyOHeNhuc0UHiKI4+zR
m+oYp8xWP4tgJKqmjqS0HTXWhk7NMcnOMSr/p331BlE7pI4Sqv7qJvu0XOrdVoJO
tRhAvJ/iyqAViGAeFSeVz3TCtQU1fHV5PyQIZ6AR3dztdmDD119ZDse86AQUOIAK
5TfDdX7nCSdYs0Qpi5weZaWZq+jCa5B5+ozB/muTwbkTAO8IJP25JzkRcQMmfOMM
3PutqqNF175YDoM+dheExec5yGlSWAILI6uZWbAHUAA8yqYvGxSie1Z7VyT/W/DW
cchlCvoaHsmOq+7fTXSUrxEAfgHEPCHTVfSLiQ==
-----END CERTIFICATE-----
```

## BLE

### Service `d839fc3c84dd4c369126187b07255129`

#### Characteristics

| uuid                               | properties                      | purpose |
| ---------------------------------- | ------------------------------- | ------- |
| `1f6b14c997fa4f1eaaa67e152fdd04f4` | 'read', 'notify', 'indicate'    | read    |
| `b378db854ec34daa828e1b99607bd6a0` | 'writeWithoutResponse', 'write' | write   |
| `f953144be33a4079b202e3d7c1f3dbb0` | 'writeWithoutResponse', 'write' | wake    |
| `22ffc5471bef48e2aa87b87e23ac0bbd` | 'read', 'notify'                | power   |
| `d353144be33a4079b202e3d7c1f3db45` | 'writeWithoutResponse', 'write' |         |

### Protocol

| command                       | module id | command id | notes                                                                                  |
| ----------------------------- | --------- | ---------- | -------------------------------------------------------------------------------------- |
| DeviceInfo                    | 01        | 01         |                                                                                        |
|                               | 01        | 0a         |                                                                                        |
| display text                  | 01        | 0b         |                                                                                        |
| StopReader                    | 01        | 0e         | assuming this means power off                                                          |
| Echo                          | 01        | fe         |                                                                                        |
| EnterProtectedMode?           | 02        | 01         |                                                                                        |
| LeaveProtectedModeUnencrypted | 02        | 02         |                                                                                        |
|                               | 02        | 04         |                                                                                        |
|                               | 02        | 05         | send certificate                                                                       |
| PrepareFileLoad               | 02        | 08         | sig of file for command 0x02/0x09                                                      |
| LoadFile                      | 02        | 09         | firmware, or localization. 512 or less chunks, maybe a boolean to indicate end of data |
| ProcessMessage                |           |            |                                                                                        |
| WaitForCard                   |           |            |                                                                                        |
| GetCardInfo                   |           |            |                                                                                        |
| CardStatus                    |           |            |                                                                                        |
| DisplayTextInsertCard         |           |            |                                                                                        |
| DisplayTextPleaseWait         |           |            |                                                                                        |
| UpdateFirmware                |           |            |                                                                                        |
| SetComParams                  |           |            |                                                                                        |
| InitTransaction               |           |            |                                                                                        |

#### Commands

##### DeviceInfo

Response:

```
SvppSoftwareVersion <Buffer 01 00 01 42>
EmvL1KernelVersion <Buffer 01 00 00 00>
EmvL2KernelVersion <Buffer 32 2e 30 30>
EmvCfg <Buffer ff ff ff ff>
AtmentSerialNumber <Buffer b2 67 86 21 08 00 d6 af fe 09 08 0c ad 00 00 00>
PartNumber(string) <Buffer ff ff ff ff>
TerminalSerialNumber <Buffer 63 a9 41 7d 34>
DisplayTextVersion <Buffer ff ff ff ff>
TrxState <Buffer 20>
MaxPayloadSize <Buffer 08 00>
LinkMduleState <Buffer 00>
BatteryState <Buffer 61>
AutomaticPowerOffTimeout <Buffer ff>
BaseFirmwareVersion <Buffer 03 00 13 62>
```

//18 EA 50 5F 4D

#### Request

##### Frame

> 02 0009 05 01fe000068656c6c6f 436a 03

- `02` is the ascii 'stx' control character,
- `0009` is length of payload,
- `05` after the 04 is the sequence number (not part of the payload).
- `436a` is a CRC 16 (ccitt) of the length, sequence, payload
- `03` at the end is 'etx'.

##### Payload

> 01 fe 0000 68656c6c6f

- 01: moduleID
- fe: commandID
- 0000... contents/parameters

#### Response

##### Frame

Send '0011000101C1C2C3C4C5C6C7C8C9CACBCCCDCECF' -> '02 0004008001fff88141 03'

- `02` is the ascii 'stx' control character,
- `0004` is length of payload,
- `00` after the 04 is the sequence number (not part of the payload).
- `8141` is a CRC 16 (ccitt) of the length, sequence, payload
- `03` at the end is 'etx'.

##### Payload

> 80010000

- 0x8001: protocol status response
- 0x0000: status
  - 0: success
  - negative numbers 0xff..): error

## USB

Same protocol as BLE, 115200 baud, 8N1, but platformio monitor can't decode some of it

### /dev

/dev/tty.usbmodemA1
/dev/cu.usbmodemA1

### lsusb

```
Bus 020 Device 037: ID fff0:0100 fff0 PinPad  Serial: A
```

### USB Prober (macos)

```
Full Speed device @ 37 (0x14120000): .............................................   Communication device: "PinPad"
    Port Information:   0x0018
           Not Captive
           External Device
           Connected
           Enabled
    Number Of Endpoints (includes EP0):
        Total Endpoints for Configuration 1 (current):   4
    Device Descriptor
        Descriptor Version Number:   0x0200
        Device Class:   2   (Communication)
        Device Subclass:   0
        Device Protocol:   0
        Device MaxPacketSize:   64
        Device VendorID/ProductID:   0xFFF0/0x0100   (unknown vendor)
        Device Version Number:   0x0100
        Number of Configurations:   1
        Manufacturer String:   1 "DATECS"
        Product String:   2 "PinPad"
        Serial Number String:   3 "A"
    Configuration Descriptor (current config)
        Length (and contents):   62
            Raw Descriptor (hex)    0000: 09 02 3E 00 02 01 00 C0  FA 09 04 00 00 01 02 02
            Raw Descriptor (hex)    0010: 00 00 05 24 00 10 01 05  24 01 00 01 04 24 02 00
            Raw Descriptor (hex)    0020: 07 05 81 03 08 00 FF 09  04 01 00 02 0A 00 00 00
            Raw Descriptor (hex)    0030: 07 05 02 02 40 00 00 07  05 83 02 40 00 00
        Number of Interfaces:   2
        Configuration Value:   1
        Attributes:   0xC0 (self-powered)
        MaxPower:   500 mA
        Interface #0 - Communications-Control
            Alternate Setting   0
            Number of Endpoints   1
            Interface Class:   2   (Communications-Control)
            Interface Subclass;   2
            Interface Protocol:   0
            Comm Class Header Functional Descriptor
                Raw Descriptor (hex)   0000: 05 24 00 10 01
            Comm Class Call Management Functional Descriptor
                Raw Descriptor (hex)   0000: 05 24 01 00 01
            Comm Class Abstract Control Management Functional Descriptor
                Raw Descriptor (hex)   0000: 04 24 02 00
            Endpoint 0x81 - Interrupt Input
                Address:   0x81  (IN)
                Attributes:   0x03  (Interrupt)
                Max Packet Size:   8
                Polling Interval:   255 ms
        Interface #1 - Communications-Data/Unknown Comm Class Model
            Alternate Setting   0
            Number of Endpoints   2
            Interface Class:   10   (Communications-Data)
            Interface Subclass;   0   (Unknown Comm Class Model)
            Interface Protocol:   0
            Endpoint 0x02 - Bulk Output
                Address:   0x02  (OUT)
                Attributes:   0x02  (Bulk)
                Max Packet Size:   64
                Polling Interval:   0 ms
            Endpoint 0x83 - Bulk Input
                Address:   0x83  (IN)
                Attributes:   0x02  (Bulk)
                Max Packet Size:   64
                Polling Interval:   0 ms

```

## iOS SDK

### su_register_firmware.bin

51720 bytes
no hits with binwalk

#### interesting `strings`

```
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/register_setup.c
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/iAP2/iAP2BuffPoolImplementation.c
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/iAP2/iAP2FSM.c
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/iAP2/iAP2Link.c
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/iAP2/iAP2LinkAccessory.c
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/iAP2/iAP2ListArray.c
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/iAP2/iAP2Packet.c
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/iAP2/iAP2Time.c
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/ctrl_handler.cpp
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/i2c.cpp
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/iap2.cpp
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/register.cpp
/Users/darthrake/Desktop/sumup-gsm-fw/user/applications/su_register/register_io.cpp
```

### SumUpSDK

#### `file`

```
SumUpSDK-arm: Mach-O universal binary with 2 architectures: [arm_v7:Mach-O dynamically linked shared library arm_v7] [arm64]
SumUpSDK-arm (for architecture armv7):  Mach-O dynamically linked shared library arm_v7
SumUpSDK-arm (for architecture arm64):  Mach-O 64-bit dynamically linked shared library arm64
SumUpSDK-x86: Mach-O universal binary with 2 architectures: [i386:Mach-O dynamically linked shared library i386] [x86_64]
SumUpSDK-x86 (for architecture i386):   Mach-O dynamically linked shared library i386
SumUpSDK-x86 (for architecture x86_64): Mach-O 64-bit dynamically linked shared library x86_64
```
