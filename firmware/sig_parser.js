const fs = require('fs');

const sig = fs.readFileSync('./svpp-1.2.28.8.sig');

const bin_length = sig.readUInt32LE(sig.length - 4);
const other_length = sig.readUInt32LE(sig.length - 8);
const diff = other_length - bin_length;
const sig_length = sig.readUInt32LE(sig.length - 12);
const content = sig.slice(2, 2 + sig_length);
const suffix = sig.slice(2 + sig_length);

console.log({
  sig_length,
  other_length,
  bin_length,
  diff,
  content: content.toString('hex'),
  suffix: suffix.toString('hex'),
  sigLength: sig.length,
});
